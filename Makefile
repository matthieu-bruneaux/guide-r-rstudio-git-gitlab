### * Variables

# Where the build takes place
SPHINX_DIR=".sphinx-forge"
# Sphinx configuration file
CONF_FILE=".conf.py"
# For conversion from org format to rst format
ORG2RST_SCRIPT=".plumbing/.org2rst.sh"
# Folder for images
IMAGE_DIR="images"
# Folder for other files (e.g. pdfs)
RESOURCES_DIR="resources"

### * Rules

### ** help

# http://swcarpentry.github.io/make-novice/08-self-doc/
# https://stackoverflow.com/questions/19129485/in-linux-is-there-a-way-to-align-text-on-a-delimiter
.PHONY: help
help: Makefile
	@echo ""
	@echo "Please use 'make <target>' where <target> is one of"
	@echo ""
	@sed -n 's/^## /    /p' $< | column -t -s ":"

### ** html

## html : render html documentation locally
.PHONY: html
html: clean
	cp $(CONF_FILE) $(SPHINX_DIR)/conf.py
	@cp *.rst $(SPHINX_DIR)
	@cp -r $(IMAGE_DIR) $(RESOURCES_DIR) $(SPHINX_DIR)
	@bash $(ORG2RST_SCRIPT) $(SPHINX_DIR)
	@cp .plumbing/.my_theme.css $(SPHINX_DIR)/_static/my_theme.css
	@cd $(SPHINX_DIR); make html
	@echo
	@echo "Build finished. The HTML pages are in $(SPHINX_DIR)/_build/html/."
	@echo "(You can visit the output pages at ./.index.html)"
	@ln -s $(SPHINX_DIR)/_build/html/index.html .index.html

### ** clean

## clean : remove all files generated or copied by Sphinx
.PHONY: clean
clean:
	rm -fr $(SPHINX_DIR)/_build
	rm -f $(SPHINX_DIR)/*.rst
	rm -fr $(SPHINX_DIR)/images
	rm -fr $(SPHINX_DIR)/resources
	rm -f $(SPHINX_DIR)/conf.py
	rm -f .index.html
