* Before we start

+ Aim:
  - Whetting one's appetite
  - Installing the required tools on your computer
  - Creating a GitLab account

+ Prerequisites:
  - None

+ Steps:
  1. The aim of the game
  2. Installing R and RStudio
  3. Installing Git
  4. Creating a GitLab account

+ Resources and references:
  - Software Carpentry lessons
  - Papers about reproducible science
  - Papers about sharing a full analysis for a paper with a notebook
  - Links from Hadley Wickham's site about packages and helping oneself

** The aim of the game

This documentation aims at presenting *tools* and *step-by-step instructions*
to help conducting *reproducible research with R*.

Reproducibility is one of the prerequisites for *open science*. The key points
addressed in this manual to help reach those goals of *reproducibility* and
*openness* are:

1. *keeping track* of your code with *version control*
2. making a *neat bundle* of your R code in a *package*
3. *documenting* and *testing* your code
4. *sharing* your code with collaborators and the word

Note that even if you do not plan to release your code publicly, the points 1
to 3 above are still extremely important to ensure that your code works as you
expect and that it is properly documented. By following the guidelines
presented in this manual, the first person you will help is very likely your
*future self*.

*** How to use this documentation

The sections of this documentation are designed to be read *one after the
other*. However, if you are looking for a specific piece of information, each
section should be explicit enough by itself.

** Installing R and RStudio

- If R is not installed on your system yet, you can follow the instructions
  from the [[https://www.r-project.org/][R project home]].

- R is totally self-sufficient, but several options exist that provide a more
  convenient interface to it. *RStudio* is an open-source software providing a
  very comfortable environment for writing R code. You can install the open
  source edition of *RStudio Desktop* from the [[https://www.rstudio.com/products/RStudio/][RStudio website]].

** Installing Git

- *Git* is a free and open source tool to keep track of the *history of a
  project folder*. Such a practice is called *version control*. You can install
  *Git* from the [[https://git-scm.com/][Git website]].

- Git is self-sufficient and can be used by itself from the command line, as
  R. However, RStudio provides an *integrated interface to Git from an R
  project* which greatly simplifies its use for the most common actions.

** Creating a GitLab account

- *Git* is the program in charge of keeping track of a project history on your
  local computer. It can also communicate with *remote hosts* to publish and
  share your code with collaborators.

- There are various third-party services providing hosting for Git
  repositories, such as:
  + [[https://github.com/][GitHub]], a very popular service which you probably already heard of
  + [[https://bitbucket.org/][Bitbucket]]
  + [[https://gitlab.com/][GitLab]]

- In this documentation, we chose to use the [[https://gitlab.com/][GitLab]] service for several reasons:
  + GitLab provides free accounts with both *private* and *public
    repositories*, which is useful when you want to keep your code within a
    restricted circle of collaborators before sharing it with the world
  + GitLab offers a continuous integration framework, without having to use
    another third-party service for it (we will learn about continuous
    integration later in the documentation)
  + Finally, GitLab is a company, offering online services at GitLab.com, but
    it is also the name of their *open source software that is freely available
    for installing on e.g. University infrastructures*. This means that if we
    learn how to use their software when hosted on their servers, we can re-use
    our knowledge to use the same software when it is installed on a University
    system or on a private server.

- You can create a free GitLab account from their [[https://gitlab.com/][website]]. *Please be sure to
  read the terms and conditions* when signing up for an account on their
  website and to decide whether or not you are happy to *host your data on
  their servers* before accepting them!

- Once you are logged in GitLab, you should set up a *SSH key pair* for easier
  and secure communication between your local repositories and their
  servers. For more information about how to set up a SSH key pair, please read
  the [[https://docs.gitlab.com/ce/ssh/README.html][GitLab help page.]]
