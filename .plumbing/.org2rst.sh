#! /bin/bash

outDir=$1
orgFiles=`ls org-pages/*.org`

for f in $orgFiles
do
    filename=${f%.*} # Remove extension
    filenameOut=${filename##*/} # Extract basename
    pandoc -f org -t rst -o $outDir/${filenameOut}.rst ${filename}.org
done
