[![pipeline status](https://gitlab.com/matthieu-bruneaux/guide-r-rstudio-git-gitlab/badges/master/pipeline.svg)](https://gitlab.com/matthieu-bruneaux/guide-r-rstudio-git-gitlab/commits/master)

# A beginner's guide to reproducible research using R, RStudio, Git and GitLab

https://matthieu-bruneaux.gitlab.io/guide-r-rstudio-git-gitlab/

## Description

This is the repository of a documentation project explaining how to set up an R
project step-by-step, in order to make it reproducible and easy to share using
RStudio and version-control tools.

## How to help

This is very much a work in progress, please feel free to contribute by testing
the instructions provided in the manual and providing feedback, comments or
suggestions!

If you would like to participate by writing or modifying content, please send a
pull request with your changes and they will be incorporated in the repository
after review.

### Writing content

The source files for each page are plain text files written
using [org mode](https://en.wikipedia.org/wiki/Org-mode). Those files are
located in the `org-pages` folder.

Images are stored in the `images` folder, which contains one subfolder for each
manual page.

## Notes

Repository avatar is *puzzle-piece* from Font Awesome by Dave Gandy
(http://fontawesome.io).
