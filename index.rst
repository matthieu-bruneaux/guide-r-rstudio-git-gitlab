Reproducible research with R, RStudio and GitLab
================================================

This manual describes how to set up an **R project** on your computer so that
it is **reproducible**, and **collaboration** between several people (including
your future self) is made easy.

The tools used in this setup are:

- R
- RStudio
- Git (for version control)
- GitLab (for online hosting of your code repository)

Detailed contents
-----------------

.. toctree::
   :maxdepth: 2

   010-before-we-start
   020-intro-version-control
   030-setting-up-gitlab-repo
   040-setting-up-R-package
   050-documentation-with-Roxygen
   060-testing
   070-continuous-integration
   075-building-website-with-pkgdown
   080-code-coverage
   090-profiling
   100-introduction-Rcpp

Contact
-------

For any comments or suggestions, or if you want to contribute to the manual,
please check out the `GitLab repository <https://gitlab.com/matthieu-bruneaux/guide-r-rstudio-git-gitlab>`_
or send an email to matthieu.bruneaux@gmail.com!
